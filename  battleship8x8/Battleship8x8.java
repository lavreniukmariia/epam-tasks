package com.epam.rd.autotasks;

public class Battleship8x8 {
    private final long ships;
    private long shots = 0L;

    public Battleship8x8(final long ships) {
        this.ships = ships;
    }

    public boolean shoot(String shot) {
        int line = 0;
        int row = 0;
        int shotLocationNum, numInCharLocation;
        char[] shotLocationChar;

        shotLocationChar = shot.toCharArray();
        numInCharLocation = Integer.parseInt(Character.toString(shotLocationChar[1]));

        String t = "";
        if(ships > 0){
            t = "0";
        }

        String shipsToString = t + Long.toBinaryString(ships);
        String shotsToString = Long.toString(shots);

        StringBuilder shipsStringBuilder = new StringBuilder(shipsToString);
        StringBuilder shotsStringBuilder = new StringBuilder(shotsToString);

        switch (shotLocationChar[0]){
            case 'A': row = 1; break;
            case 'B': row = 2; break;
            case 'C': row = 3; break;
            case 'D': row = 4; break;
            case 'E': row = 5; break;
            case 'F': row = 6; break;
            case 'G': row = 7; break;
            case 'H': row = 8; break;
        }
        switch (numInCharLocation){
            case 1:
                break;
            case 2: line = 8; break;
            case 3: line = 16; break;
            case 4: line = 24; break;
            case 5: line = 32; break;
            case 6: line = 40; break;
            case 7: line = 48; break;
            case 8: line = 56; break;
        }
        shotLocationNum = line + row - 1;
        if(shots == 0){
            shotsStringBuilder.deleteCharAt(0);
        }
        if(shotLocationNum < 10){
            shotsStringBuilder.append("8" + shotLocationNum);
        }
        else {
            shotsStringBuilder.append(shotLocationNum);
        }

        shots = Long.parseLong(shotsStringBuilder.toString());

        if(shipsStringBuilder.charAt(shotLocationNum) == '1'){
            return true;
        }
        else {
            return false;
        }
    }

    public String state() {
        String t = "";
        if(ships > 0){
            t = "0";
        }
        String shipsToString = t + Long.toBinaryString(ships);
        String shotsToString = Long.toString(shots);

        StringBuilder shipsStringBuilder = new StringBuilder(shipsToString);
        StringBuilder shotsStringBuilder = new StringBuilder(shotsToString);

        String[] shotsStringNum = new String[shotsToString.length()/2];
        int[] xNums = new int[shotsStringNum.length];
        int j = 0;
        for (int i = 0; i < shotsToString.length(); i+=2){
            String char1 = Character.toString(shotsStringBuilder.charAt(i));
            String char2 = Character.toString(shotsStringBuilder.charAt(i+1));
            if(i%2 == 0) {
                if(char1.contains("8")){
                    char1 = char1.replaceAll("8", "");
                }
                    shotsStringNum[j] = char1 + char2;
                    j++;
            }
        }
        for(int i = 0; i < shotsStringNum.length; i++){
            xNums[i] = Integer.parseInt(shotsStringNum[i]);
            if(shipsStringBuilder.charAt(xNums[i]) == '1'){
                shipsStringBuilder.setCharAt(xNums[i], '☒');
            }
            else if(shipsStringBuilder.charAt(xNums[i]) == '0'){
                shipsStringBuilder.setCharAt(xNums[i], '×');
            }
        }
        String resultWithoutSlash = shipsStringBuilder.toString();
        resultWithoutSlash = resultWithoutSlash.replaceAll("0", ".");
        resultWithoutSlash = resultWithoutSlash.replaceAll("1", "☐");

        StringBuilder nearlyResult = new StringBuilder(resultWithoutSlash);
        StringBuilder resultStringBuilder = new StringBuilder("");
        int n = 8;
        int k = 0;
        int l = resultWithoutSlash.length();
        for (int i = 0; i < l+7; i++){
            if(i == n ){
                resultStringBuilder.append("\n");
                n = n+9;
            }
            else{
                resultStringBuilder.append(nearlyResult.charAt(k));
                k++;
            }
        }
        String result = resultStringBuilder.toString();
        return result;
    }
}
