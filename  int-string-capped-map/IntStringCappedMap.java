package com.epam.autotasks.collections;

import java.util.*;

class IntStringCappedMap extends AbstractMap<Integer, String> {
    private final long capacity;

    private final TreeMap<Integer, String> map;
    private final Queue<Integer> queue;

    public IntStringCappedMap(final long capacity) {
        this.capacity = capacity;
       // map = new HashMap<>();
        this.map = new TreeMap<>();
        this.queue = new ArrayDeque<>();
    }

    public long getCapacity() {
        return capacity;
    }

    @Override
    public Set<Entry<Integer, String>> entrySet() {
        return new AbstractSet<>() {
            @Override
            public Iterator<Entry<Integer, String>> iterator() {
                return new Iterator<>() {
                    final Iterator<Entry<Integer, String>> iterator = map.entrySet().iterator();
                    @Override
                    public boolean hasNext() {
                        //implement this method
                        return iterator.hasNext();
                        //throw new UnsupportedOperationException();
                    }

                    @Override
                    public Entry<Integer, String> next() {
                        //implement this method
                        return iterator.next();
                        //throw new UnsupportedOperationException();
                    }
                };
            }

            @Override
            public int size() {
                return IntStringCappedMap.this.size();
            }
        };
    }

    @Override
    public String get(final Object key) {
        //implement this method
        return map.get(key);
        //throw new UnsupportedOperationException();
    }

    @Override
    public String put(final Integer key, final String value) {
        //implement this method
        long currentCapacity = 0;
        for(Map.Entry<Integer, String> m: map.entrySet()){
            currentCapacity = currentCapacity + m.getValue().length();
        }
        if(queue.size() != map.size()){
            queue.remove(0);
        }
        if(map.containsKey(key) && (value.length() - map.get(key).length() <= (capacity - currentCapacity))){
            queue.remove(key);
            queue.add(key);
            return map.put(key, value);
        }
        if(value.length() > capacity){
            throw new IllegalArgumentException();
        }
        while (currentCapacity + value.length() > capacity){
            map.remove(queue.poll());
            currentCapacity = 0;
            for(Map.Entry<Integer, String> m: map.entrySet()){
                currentCapacity = currentCapacity + m.getValue().length();
            }
            if(map.containsKey(key) && (value.length() - map.get(key).length() <= (capacity - currentCapacity))){
                queue.remove(key);
                queue.add(key);
                return map.put(key, value);
            }
        }
        queue.add(key);
        return map.put(key, value);
        //throw new UnsupportedOperationException();
    }

    @Override
    public String remove(final Object key) {
        //implement this method
        return map.remove(key);
        //throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        //implement this method
        return map.size();
        //throw new UnsupportedOperationException();
    }

}
