package com.efimchick.ifmo;

import com.efimchick.ifmo.util.CourseResult;
import com.efimchick.ifmo.util.Person;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Collecting {
    int maxCol;
    String space = " ";

    public int sum(IntStream limit) {
        return limit.sum();
    }

    public int production(IntStream limit) {
        int res = 1;
        int[] limitArray = limit.toArray();
        for (int j : limitArray) {
            res *= j;
        }
        return res;
    }

    public int oddSum(IntStream limit) {
        int res = 0;
        int[] limitArray = limit.toArray();
        for (int j : limitArray) {
            if (j % 2 != 0) {
                res += j;
            }
        }
        return res;
    }

    public Map<Integer, Integer> sumByRemainder(int i, IntStream limit) {
        Map<Integer, Integer> map = new HashMap<>();
        int[] limitArray = limit.toArray();
        int res, remainder;
        for (int k : limitArray) {
            remainder = k % i;
            res = k;
            if (map.containsKey(remainder)) {
                res += map.get(remainder);
            }
            map.put(remainder, res);
        }
        return map;
    }

    public Map<Person, Double> totalScores(Stream<CourseResult> programmingResults) {
        Map<Person, Double> map = new HashMap<>();
        List<CourseResult> courseResultList = programmingResults.collect(Collectors.toList());

        List<Person> personList = courseResultList.stream().map(CourseResult::getPerson).collect(Collectors.toList());
        List<Map<String, Integer>> valuesMapList = courseResultList.stream().map(CourseResult::getTaskResults).collect(Collectors.toList());

        Iterator<Map<String, Integer>> iteratorMap = valuesMapList.iterator();
        Iterator<Person> iteratorPerson = personList.iterator();

        Set<String> subjects = new HashSet<>();

        for (Map<String, Integer> m : valuesMapList) {
            subjects.addAll(m.keySet());
        }

        while (iteratorMap.hasNext() && iteratorPerson.hasNext()) {
            Double sum = 0.0;
            Map<String, Integer> temp = iteratorMap.next();
            for (String key : temp.keySet()) {
                sum += temp.get(key);
            }
            sum = sum / subjects.size();
            map.put(iteratorPerson.next(), sum);
        }
        return map;
    }

    public double averageTotalScore(Stream<CourseResult> programmingResults) {
        double res = 0;
        List<CourseResult> courseResultList = programmingResults.collect(Collectors.toList());
        List<Map<String, Integer>> valuesMapList = courseResultList.stream().map(CourseResult::getTaskResults).collect(Collectors.toList());
        Set<String> subjectName = new HashSet<>();
        List<Integer> results = new ArrayList<>();

        for (Map<String, Integer> m : valuesMapList) {
            subjectName.addAll(m.keySet());
            for (String key : m.keySet()) {
                results.add(m.get(key));
            }
        }
        for (Integer r : results) {
            res += r;
        }
        res = res / (subjectName.size() * valuesMapList.size());
        return res;
    }

    public Map<String, Double> averageScoresPerTask(Stream<CourseResult> programmingResults) {
        Map<String, Double> map = new HashMap<>();
        Map<String, Double> tempMap = new HashMap<>();
        double res;
        List<CourseResult> courseResultList = programmingResults.collect(Collectors.toList());

        List<Map<String, Integer>> valuesMapList = courseResultList.stream().map(CourseResult::getTaskResults).collect(Collectors.toList());
        List<Person> personList = courseResultList.stream().map(CourseResult::getPerson).collect(Collectors.toList());

        for (Map<String, Integer> m : valuesMapList) {
            for (String key : m.keySet()) {
                res = Double.valueOf(m.get(key));
                if (tempMap.containsKey(key)) {
                    res += tempMap.get(key);
                }
                tempMap.put(key, res);
            }
        }

        for (String key : tempMap.keySet()) {
            double averageRes = tempMap.get(key) / personList.size();
            map.put(key, averageRes);
        }
        return map;
    }

    public Map<Person, String> defineMarks(Stream<CourseResult> programmingResults) {
        Map<Person, Double> scoresMap = totalScores(programmingResults);
        Map<Person, String> map = new HashMap<>();
        for (Person key : scoresMap.keySet()) {
            Double d = scoresMap.get(key);
            if (d > 90 && d <= 100) {
                map.put(key, "A");
            } else if (d >= 83 && d <= 90) {
                map.put(key, "B");
            } else if (d >= 75 && d < 83) {
                map.put(key, "C");
            } else if (d >= 68 && d < 75) {
                map.put(key, "D");
            } else if (d >= 60 && d < 68) {
                map.put(key, "E");
            } else if (d < 60) {
                map.put(key, "F");
            }
        }
        return map;
    }

    public String defineMarks(double d) {
        String res = "";
        if (d > 90 && d <= 100) {
            res = "A";
        } else if (d >= 83 && d <= 90) {
            res = "B";
        } else if (d >= 75 && d < 83) {
            res = "C";
        } else if (d >= 68 && d < 75) {
            res = "D";
        } else if (d >= 60 && d < 68) {
            res = "E";
        } else if (d < 60) {
            res = "F";
        }
        return res;
    }

    public String easiestTask(Stream<CourseResult> programmingResults) {
        Map<String, Double> map = averageScoresPerTask(programmingResults);
        String res = map.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue()
                ).get().getKey();
        return res;
    }

    public String studentResults(List<CourseResult> list) {
        // res - result String
        StringBuilder res;
        Integer i = 0;

        // resMap - Map of Strings for res
        Map<Integer, String> resMap = new HashMap<>();

        Set<String> subjects = new HashSet<>();

        List<Map<String, Integer>> subjectMark = list.stream().map(CourseResult::getTaskResults).collect(Collectors.toList());
        for(Map<String, Integer> m: subjectMark){
            subjects.addAll(m.keySet());
        }
        List<Person> person = list.stream().map(CourseResult::getPerson).collect(Collectors.toList());

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Person and his total mark
        Map<String, Double> personTotal = new HashMap<>();
        for (Person key : totalScores(list.stream()).keySet()) {
            personTotal.put(key.getLastName() + " " + key.getFirstName(), totalScores(list.stream()).get(key));
        }
        Map<String, Double> personTotalSorted = personTotal.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        Map<Person, Map<String, Integer>> nameSubjectMarkPerson = new HashMap<>();
        Iterator<Map<String, Integer>> iteratorNameSubj = subjectMark.iterator();
        for (Person key : person) {
            nameSubjectMarkPerson.put(key, iteratorNameSubj.next());
        }

        Map<String, Map<String, Integer>> nameSubjectMark = new HashMap<>();
        for (Person key : nameSubjectMarkPerson.keySet()) {
            nameSubjectMark.put(key.getLastName() + " " + key.getFirstName(), nameSubjectMarkPerson.get(key));
        }

        for (String m: nameSubjectMark.keySet()){
            Map<String, Integer> map = nameSubjectMark.get(m);
            for(String s: subjects){
                if(!map.containsKey(s)){
                    map.put(s, 0);
                }
            }
            nameSubjectMark.put(m, map);

        }
        Map<String, Map<String, Integer>> nameSubjectMarkSorted = nameSubjectMark.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        for (String key : nameSubjectMarkSorted.keySet()) {
            Map<String, Integer> tempSubjectMark = nameSubjectMarkSorted.get(key);
            Map<String, Integer> tempSubjectMarkSorted = tempSubjectMark.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            nameSubjectMarkSorted.put(key, tempSubjectMarkSorted);
        }

        Map<String, String> personDefineMark = new HashMap<>();
        for (Person key : defineMarks(list.stream()).keySet()) {
            personDefineMark.put(key.getLastName() + " " + key.getFirstName(), defineMarks(list.stream()).get(key));
        }
        Map<String, String> personDefineMarkSorted = personDefineMark.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        int maxLength = 0;
        Map<String, Integer> tableSubjMark = new HashMap<>();

        for (String keyName : nameSubjectMarkSorted.keySet()) {
            if (keyName.length() > maxLength) {
                maxLength = keyName.length() + 1 ;
                maxCol = maxLength;
            }
        }

        String space = " ";
        for (String keyName : nameSubjectMarkSorted.keySet()) {
            StringBuilder addingSpace = new StringBuilder();
            if(keyName.length() < maxLength){
                int l = maxLength - keyName.length();
                addingSpace.append(space.repeat(l));
            }
            resMap.put(i, keyName + addingSpace + "| ");
            tableSubjMark = nameSubjectMarkSorted.get(keyName);
            int maxSubj;
            int addLength;
            for (String keySubj : tableSubjMark.keySet()) {
                maxSubj = keySubj.length();
                addLength = maxSubj - tableSubjMark.get(keySubj).toString().length();
                String spaceColTwo = space.repeat(Math.max(0, addLength));
                resMap.put(i, resMap.get(i) + spaceColTwo + tableSubjMark.get(keySubj) + " | ");
            }
            DecimalFormat df = new DecimalFormat("##.##");
            String t = df.format(personTotalSorted.get(keyName));
            if (t.contains(",")) {
                t = t.replace(',', '.');
            }
            else if (t.length() < 5){
                t += ".00";
            }
            resMap.put(i, resMap.get(i) + t + " |    " + personDefineMarkSorted.get(keyName) + " |\n");
            i++;
        }

        res = new StringBuilder("Student");
        int add = maxCol - res.length();
        res.append(space.repeat(Math.max(0, add))).append("| ");
        for (String keySubj : tableSubjMark.keySet()) {
            res.append(keySubj).append(" | ");
        }
        res.append("Total | Mark |\n");
        for (Integer keyRes : resMap.keySet()) {
            res.append(resMap.get(keyRes));
        }
        return res.toString();
    }

    public String average(List<CourseResult> list) {
        String res = "Average";
        int add = maxCol - res.length();
        res += space.repeat(Math.max(0, add)) + "| ";
        double avg = 0;

        Map<String, Double> averageScores = averageScoresPerTask(list.stream());
        Map<String, Double> averageScoresSorted = averageScores.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        // Scores
        for (String key : averageScoresSorted.keySet()) {
            int max = key.length();
            avg += averageScoresSorted.get(key);
            DecimalFormat df = new DecimalFormat("##.##");
            String t = df.format(averageScoresSorted.get(key));
            if (t.contains(",")) {
                t = t.replace(',', '.');
            } else if (t.length() < 5){
                t += ".00";
            }
            int addingo = max - t.length();
            res += String.valueOf(space).repeat(Math.max(0, addingo)) + t + " | ";
        }
        avg = avg / averageScoresSorted.size();
        DecimalFormat df = new DecimalFormat("##.##");
        String t = df.format(avg);
        if (t.contains(",")) {
            t = t.replace(',', '.');
        } else if (t.length() < 5 && t.length() != 4){
            t += ".00";
        }
        if(t.contains(".") && t.length() < 5){
            t += "0";
        }
        res += t + " |    " + defineMarks(avg) + " |";
        return res;
    }

    public Collector<CourseResult, List<CourseResult>, String> printableStringCollector() {
        return new Collector<>() {
            @Override
            public Supplier<List<CourseResult>> supplier() {
                return ArrayList::new;
            }

            @Override
            public BiConsumer<List<CourseResult>, CourseResult> accumulator() {
                return List::add;
            }

            @Override
            public BinaryOperator<List<CourseResult>> combiner() {
                return (list1, list2) -> {
                    list1.addAll(list2);
                    return list1;
                };
            }

            @Override
            public Function<List<CourseResult>, String> finisher() {
                return list -> studentResults(list) + average(list);
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Set.of(Characteristics.UNORDERED);
            }
        };
    }
}
