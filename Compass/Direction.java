package com.epam.rd.autotasks;

import static java.lang.Math.abs;

public enum Direction {
    N(0), NE(45), E(90), SE(135), S(180), SW(225), W(270), NW(315);

    Direction(final int degrees) {
        this.degrees = degrees;
    }

    private final int degrees;

    public static Direction ofDegrees(int degrees) {
        if(degrees < 0){
            degrees = 360 + degrees;
        }
        if(degrees >= 360){
            int num = degrees/360;
            int minus = num*360;
            degrees = degrees-minus;
        }
        for (Direction dir: Direction.values()){
            if(dir.degrees == degrees){
                return dir;
            }
        }
        return null;
    }

    public static Direction closestToDegrees(int degrees) {
        if(degrees < 0){
            degrees = 360 + degrees;
        }
        if(degrees >= 360){
            int num = degrees/360;
            int minus = num*360;
            degrees = degrees-minus;
        }
        for (Direction dir: Direction.values()){
            int middle = dir.degrees + 23;
            if(degrees < middle){
                return dir;
            }
        }
        return null;
    }

    public Direction opposite() {
        int degree = this.degrees + 180;
        if(degree < 0){
            degree = 360 + degree;
        }
        if(degree >= 360){
            int num = degree/360;
            int minus = num*360;
            degree = degree-minus;
        }
        for (Direction dir: Direction.values()){

            if(dir.degrees == degree){
                return dir;
            }
        }
        return null;
    }

    public int differenceDegreesTo(Direction direction) {
        int diff = abs(this.degrees - direction.degrees);
        if(this.degrees == 0 && direction == NW){
            return 360- direction.degrees;
        }
        return diff;
    }
}
