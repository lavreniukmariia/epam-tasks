package com.epam.rd.autotasks;

class CycleSwap {
    static void cycleSwap(int[] array) {
        if(array.length != 0) {
            int last = array[array.length - 1];
            for (int i = array.length - 1; i > 0; i--) {
                array[i] = array[i - 1];
            }
            array[0] = last;
        }
    }

    static void cycleSwap(int[] array, int shift) {
        if(array.length != 0) {
            int[] cont = new int[shift];
            int temp = 0;
            for (int i = array.length - shift; i < array.length; i++) {
                cont[temp] = array[i];
                temp++;
            }
            for (int i = array.length - 1; i >= shift; i--) {
                array[i] = array[i - shift];
            }
            for (int i = 0; i < shift; i++) {
                array[i] = cont[i];
            }
        }
    }
}
