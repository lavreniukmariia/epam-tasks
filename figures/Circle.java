package com.epam.rd.autotasks.figures;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

class Circle extends Figure{
    Point center;
    double radius;
    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public double area(){
        return PI*pow(radius,2);
    }

    public String pointsToString(){
        return "(" + center.getX() + "," + center.getY() + ")";
    }

    public String toString(){
        return this.getClass().getSimpleName() + "[" + pointsToString() + radius + "]";
    }

    public Point leftmostPoint(){
        double x = center.getX() - radius;
        return new Point(x,center.getY());
    }
}
