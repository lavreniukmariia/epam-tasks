package com.epam.rd.autotasks.figures;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

class Quadrilateral extends Figure {
    Point a, b, c, d;
    double abLength, bcLength, cdLength, daLength, acLength;

    public Quadrilateral(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public double area() {
        abLength = sqrt(pow(b.getX() - a.getX(), 2) + pow(b.getY() - a.getY(), 2));
        bcLength = sqrt(pow(c.getX() - b.getX(), 2) + pow(c.getY() - b.getY(), 2));

        cdLength = sqrt(pow(d.getX() - c.getX(), 2) + pow(d.getY() - c.getY(), 2));
        daLength = sqrt(pow(a.getX() - d.getX(), 2) + pow(a.getY() - d.getY(), 2));

        acLength = sqrt(pow(c.getX() - a.getX(), 2) + pow(c.getY() - a.getY(), 2));

        double pABC = (abLength + bcLength + acLength) / 2;
        double pCDA = (cdLength + daLength + acLength) / 2;

        double first = abs(sqrt(pABC * (pABC - abLength) * (pABC - bcLength) * (pABC - acLength)));
        double second = abs(sqrt(pCDA * (pCDA - cdLength) * (pCDA - daLength) * (pCDA - acLength)));

        return first + second;
    }

    public String pointsToString() {
        return "(" + a.getX() + "," + a.getY() + ")" + "(" + b.getX() + "," + b.getY() + ")" + "(" + c.getX() + "," + c.getY() + ")" + "(" + d.getX() + "," + d.getY() + ")";
    }

    public Point leftmostPoint() {
        if (a.getX() <= b.getX() && a.getX() <= c.getX() && a.getX() <= d.getX()) {
            return new Point(a.getX(), a.getY());
        }
        else if (b.getX() <= a.getX() && b.getX() <= c.getX() && b.getX() <= d.getX()) {
            return new Point(b.getX(), b.getY());
        }
        else if (c.getX() <= a.getX() && c.getX() <= b.getX() && c.getX() <= d.getX()) {
            return new Point(c.getX(), c.getY());
        }
        else if (d.getX() <= a.getX() && d.getX() <= b.getX() && d.getX() <= c.getX()) {
            return new Point(d.getX(), d.getY());
        }
        else {
            return new Point(0,0);
        }
    }

}
