package com.epam.rd.autotasks.figures;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

class Triangle extends Figure {
    Point a, b, c;
    double abLength, bcLength, caLength;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double area() {
        abLength = sqrt(pow(b.getX() - a.getX(), 2) + pow(b.getY() - a.getY(), 2));
        bcLength = sqrt(pow(c.getX() - b.getX(), 2) + pow(c.getY() - b.getY(), 2));
        caLength = sqrt(pow(a.getX() - c.getX(), 2) + pow(a.getY() - c.getY(), 2));
        double p = (abLength + bcLength + caLength) / 2;
        return abs(sqrt(p * (p - abLength) * (p - bcLength) * (p - caLength)));
    }

    public String pointsToString() {
        return "(" + a.getX() + "," + a.getY() + ")" + "(" + b.getX() + "," + b.getY() + ")" + "(" + c.getX() + "," + c.getY() + ")";
    }

    public Point leftmostPoint() {
        if (a.getX() <= b.getX() && a.getX() <= c.getX()) {
            return new Point(a.getX(), a.getY());
        }
        else if (b.getX() <= a.getX() && b.getX() <= c.getX()) {
            return new Point(b.getX(), b.getY());
        }
        else if (c.getX() <= a.getX() && c.getX() <= b.getX()) {
            return new Point(c.getX(), c.getY());
        }
        else {
            return new Point(0,0);
        }
    }

}
