package com.epam.rd.autotasks.intersection;

public class Line {
    int k, b;
    public Line(int k, int b) {
        this.k = k;
        this.b = b;
    }

    public Point intersection(Line other) {
        if(this.k == other.k && this.b == other.b){
            return null;
        }
        else if(this.k - other.k != 0){
            int x = (other.b - this.b) / (this.k - other.k);
            int y = this.k * x + this.b;
            return new Point(x, y);
        }
        else {
            return null;
        }
    }

}
