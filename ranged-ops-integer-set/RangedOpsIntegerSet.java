package com.epam.autotasks.collections;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

class RangedOpsIntegerSet extends AbstractSet<Integer> {
    private final Set<Integer> set;

    RangedOpsIntegerSet() {
        set = new TreeSet<>();
    }

    public boolean add(int fromInclusive, int toExclusive) {
        boolean res, temp = false;
        for (int i = fromInclusive; i < toExclusive; i++){
            res = set.add(i);
            if(res){
                temp = true;
            }
        }
        return temp;
    }

    public boolean remove(int fromInclusive, int toExclusive) {
        boolean res, temp = false;
        for (int i = fromInclusive; i < toExclusive; i++){
            res = set.remove(i);
            if(res){
                temp = true;
            }
        }
        return temp;
    }

    @Override
    public boolean add(final Integer integer) {
        return set.add(integer);
    }

    @Override
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<Integer> iterator() {
        return set.iterator();
    }

    @Override
    public int size() {
        return set.size();
    }
}
