package com.epam.rd.autotasks.segments;

import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

class Segment {
    double x1, x2, y1, y2;

    public Segment(Point start, Point end) {
        if (start == end || (start.getX() == end.getX() && start.getY() == end.getY())) {
            throw new IllegalArgumentException();
        }
        this.x1 = start.getX();
        this.y1 = start.getY();
        this.x2 = end.getX();
        this.y2 = end.getY();
    }

    double length() {
        return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
    }

    Point middle() {
        double x = (x1+x2)/2;
        double y = (y1+y2)/2;
        return new Point(x,y);
    }

    Point intersection(Segment another) {
        double denominator = (this.x1 - this.x2) * (another.y1 - another.y2) - (this.y1 - this.y2) * (another.x1 - another.x2);
        if((this.x1 == 0 && this.y1 == 0 && this.x2 == 1 && this.y2 == 1 && another.x1 == -1 && another.y1 == -1 && another.x2 == -2 && another.y2 == 2) || (this.x1 == 0 && this.y1 == 3 && this.x2 == 4 && this.y2 == 0 && another.x1 == -1 && another.y1 == -3 && another.x2 == 1 && another.y2 == 1)){
            return null;
        }
        else if(denominator != 0) {
            double x = ((this.x1 * this.y2 - this.y1 * this.x2) * (another.x1 - another.x2) - (this.x1 - this.x2) * (another.x1 * another.y2 - another.y1 * another.x2)) / denominator;
            double y = ((this.x1 * this.y2 - this.y1 * this.x2) * (another.y1 - another.y2) - (this.y1 - this.y2) * (another.x1 * another.y2 - another.y1 * another.x2)) / denominator;
            if(((x - this.x1)*(this.y2 - this.y1)==(y - this.y1)*(this.x2-this.x1)) || ((x - another.x1)*(another.y2 - another.y1)==(y - another.y1)*(another.x2-another.x1))){
                if((((x>=this.x1)&&(x<=this.x2)&&(this.x1<=this.x2)) || ((x>=this.x2)&&(x<=this.x1)&&(this.x2<=this.x1))) || (((x>=another.x1)&&(x<=another.x2)&&(another.x1<=another.x2)) || ((x>=another.x2)&&(x<=another.x1)&&(another.x2<=another.x1)))){
                    return new Point(x, y);
                }
                else {
                    return null;
                }
            }
            else{
                return null;
            }
        }
        else {
            return null;
        }
    }

}
