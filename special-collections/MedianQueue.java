package com.efimchick.ifmo.collections;

import java.util.*;

class MedianQueue implements Queue<Integer> {
    private final Queue<Integer> queue;

    public MedianQueue() {
        queue = new ArrayDeque<>();
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public Iterator<Integer> iterator() {
        return queue.iterator();
    }

    @Override
    public Integer poll() {
        return queue.poll();
    }

    @Override
    public Integer peek() {
        int medianIndex, leftElement, rightElement, size;
        Integer median;

        List<Integer> list = new LinkedList<>();
        list.addAll(queue);
        Collections.sort(list);
        size = list.size();

        if(size % 2 != 0){
            leftElement = size / 2 - 1;
            rightElement = size / 2 + 1;
            medianIndex = size / 2;
            median = list.get(medianIndex);

            queue.clear();
            queue.add(median);

            for (int i = 0; i < size - 1; i++){
                if(i % 2 == 0){
                    queue.add(list.get(leftElement));
                    leftElement--;
                }
                else {
                    queue.add(list.get(rightElement));
                    rightElement++;
                }
            }
        }
        else {
            leftElement = size / 2 - 2;
            rightElement = size / 2;
            medianIndex = size / 2 - 1;
            median = list.get(medianIndex);

            queue.clear();
            queue.add(median);

            for (int i = 0; i < size - 1; i++){
                if(i % 2 == 0){
                    queue.add(list.get(rightElement));
                    rightElement++;
                }
                else {
                    queue.add(list.get(leftElement));
                    leftElement--;
                }
            }
        }
        return queue.peek();
    }

    @Override
    public boolean offer(Integer integer) {
        return queue.offer(integer);
    }




    @Override
    public Integer element() {
        return null;
    }

    @Override
    public Integer remove() {
        return null;
    }

    @Override
    public void clear() {}

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }
}
