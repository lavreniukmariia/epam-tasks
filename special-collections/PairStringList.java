package com.efimchick.ifmo.collections;

import java.util.*;

class PairStringList extends AbstractList<String> {
    private final ArrayList<String> list;

    public PairStringList() {
        list = new ArrayList<>();
    }

    @Override
    public boolean add(String s) {
        list.add(s);
        return list.add(s);
    }

    @Override
    public void add(int index, String element) {
        if(index % 2 == 0){
            list.add(index, element);
            list.add(index, element);
        }
        else {
            list.add(index+1, element);
            list.add(index+1, element);
        }
    }

    @Override
    public boolean remove(Object o) {
        list.remove(o);
        return list.remove(o);
    }

    @Override
    public String remove(int index) {
        if(index % 2 == 0){
            list.remove(index + 1);
        }
        else {
            list.remove(index);
        }
        return list.remove(index - 1);
    }

    @Override
    public String get(int index) {
        return list.get(index);
    }

    @Override
    public String set(int index, String element) {
        if(index % 2 == 0){
            list.set(index, element);
            return list.set(index + 1, element);
        }
        else {
            list.set(index, element);
            return list.set(index-1, element);
        }
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        for (String s: c){
            list.add(s);
            list.add(s);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        int i;
        ArrayDeque<String> queue = new ArrayDeque<>();
        queue.addAll(c);

        if(index % 2 != 0) {
            i = index + 1;
            for (String q : queue) {
                list.add(i, q);
                i++;
                list.add(i, q);
                i++;
                queue.poll();
            }
        }
        else {
            i = index;
            for (String q : queue) {
                list.add(i, q);
                i++;
                list.add(i, q);
                i++;
                queue.poll();
            }
        }

        return true;
    }

    @Override
    public Iterator<String> iterator() {
        return list.iterator();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.size() == 0;
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public void clear() {
        list.clear();
    }
}
