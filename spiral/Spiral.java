package com.epam.rd.autotasks;

class Spiral {
    static int[][] spiral(int rows, int columns) {
        int[][] mas = new int[rows][columns];
        int jUpLeft = 0;
        int iUpRight = 0;
        int jDownRight = columns-1;
        int iDownLeft = rows-1;

        int num = 1;

        while(jUpLeft <= jDownRight && iUpRight <= iDownLeft) {
            for (int j = jUpLeft; j <= jDownRight; j++) {
                mas[iUpRight][j] = num;
                num++;
            }
            iUpRight++;
            if(num <= rows*columns) {
                for (int i = iUpRight; i <= iDownLeft; i++) {
                    mas[i][jDownRight] = num;
                    num++;
                }
                jDownRight--;

                for (int j = jDownRight; j >= jUpLeft; j--) {
                    mas[iDownLeft][j] = num;
                    num++;
                }
                iDownLeft--;

                for (int i = iDownLeft; i >= iUpRight; i--) {
                    mas[i][jUpLeft] = num;
                    num++;
                }
                jUpLeft++;
            }
        }
        return mas;
    }
}
