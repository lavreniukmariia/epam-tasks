package com.efimchick.ifmo.streams.countwords;


import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Words {

    public String countWords(List<String> lines) {
        List<String> newList = lines.stream().map(x -> x.replaceAll("[^\\p{L}]", " ")).collect(toList());
        String onlyWords = String.join(" ", newList).toLowerCase(Locale.ROOT);

        List<String> allWords = new ArrayList<>(Arrays.asList(onlyWords.split(" ")))
                .stream().filter(x -> x.length() >= 4)
                .filter(x -> !x.contains("'"))
                .collect(Collectors.toList());

        Map<String, Long> words =
                allWords.stream().collect(groupingBy(Function.identity(), counting()));

        Map<String, Long> sortedByKey = words.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .filter(x -> x.getValue() >= 10)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        Map<String, Long> sortedByValue = sortedByKey.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        String res = sortedByValue.keySet().stream().map(key -> key + " - " + words.get(key) + "\n").collect(joining()).stripTrailing();
        return res;
    }
}
