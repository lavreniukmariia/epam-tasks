package com.epam.rd.autotasks.triangle;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;

class Triangle {
    double kLength, nLength, sLength;
    Point abMiddle, bcMiddle;
    Point a, b, c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
        kLength = sqrt(pow(b.getX() - a.getX(), 2) + pow(b.getY() - a.getY(), 2));
        nLength = sqrt(pow(c.getX() - b.getX(), 2) + pow(c.getY() - b.getY(), 2));
        sLength = sqrt(pow(a.getX() - c.getX(), 2) + pow(a.getY() - c.getY(), 2));
        double max;
        if (kLength > nLength && kLength > sLength) {
            max = kLength;
            if (max > nLength + sLength) {
                throw new IllegalArgumentException();
            }
        } else if (nLength > kLength && nLength > sLength) {
            max = nLength;
            if (max > kLength + sLength) {
                throw new IllegalArgumentException();
            }
        } else if (sLength > kLength && sLength > nLength) {
            max = sLength;
            if (max > kLength + nLength) {
                throw new IllegalArgumentException();
            }
        }
        if(this.area() == 0){
            throw new IllegalArgumentException();
        }
    }

    public double area() {
        double p = (kLength + nLength + sLength) / 2;
        return abs(sqrt(p * (p - kLength) * (p - nLength) * (p - sLength)));
    }

    public Point centroid() {
        double abX = (a.getX() + b.getX()) / 2;
        double abY = (a.getY() + b.getY()) / 2;
        double bcX = (b.getX() + c.getX()) / 2;
        double bcY = (b.getY() + c.getY()) / 2;
        abMiddle = new Point(abX, abY);
        bcMiddle = new Point(bcX, bcY);
        double denominator = (abX - c.getX())*(bcY - a.getY()) - (abY - c.getY())*(bcX - a.getX());
        double x = ((abX*c.getY() - abY*c.getX())*(bcX - a.getX()) - (abX - c.getX())*(bcX*a.getY() - bcY*a.getX()))/denominator;
        double y = ((abX*c.getY() - abY*c.getX())*(bcY - a.getY()) - (abY - c.getY())*(bcX*a.getY() - bcY*a.getX()))/denominator;
        return new Point(x, y);
    }

}
