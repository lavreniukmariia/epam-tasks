import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColorCodeValidation {
    public static boolean validateColorCode(String color) {
        if(color == null || color.contains("h") || color.contains("g") || color.contains("x")){
            return false;
        }
        Pattern pattern6 = Pattern.compile("\\#\\w{6}");
        Pattern pattern3 = Pattern.compile("\\#\\w{3}");
        Matcher matcher6 = pattern6.matcher(color);
        Matcher matcher3 = pattern3.matcher(color);
        int length = color.length();
        if(length != 4 && length != 7){
            return false;
        }
        return matcher6.find() || matcher3.find();
    }
}
