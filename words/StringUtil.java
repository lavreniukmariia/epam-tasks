package com.epam.rd.autotasks.words;

import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static int countEqualIgnoreCaseAndSpaces(String[] words, String sample) {
        if(words == null || sample == null){
            return 0;
        }
        int count = 0;
        sample = sample.strip();
        sample = sample.toLowerCase(Locale.ROOT);
        for(String w: words){
            w = w.replaceAll("\\t","");
            w = w.strip();
            if(w.equalsIgnoreCase(sample)){
                count++;
            }
        }
        return count;
    }

    public static String[] splitWords(String text) {
        if(text == null){
            return null;
        }
        text = text.replaceAll("\\W+", " ");
        text = text.strip();
        String[] strArr = text.split("\\s");
        String str = Arrays.toString(strArr);
        str = str.replaceAll("\\W+","");
        if(str.isEmpty()){
            return null;
        }
        else {
            return strArr;
        }
    }

    public static String convertPath(String path, boolean toWin) {
        if(path == null || path.isEmpty()){
            return null;
        }
        if((path.contains("/") && path.contains("\\")) || path.contains("/~/") || path.contains("///") || path.contains("~\\")
                || path.contains("~/~") || path.contains("~~") || path.contains("\\C:\\")){
            return null;
        }
        if(toWin){
            Pattern patternRoot = Pattern.compile("(C:\\\\|\\\\|\\.\\\\|(\\.\\.\\\\))?((\\w+|(\\.\\.))(\\\\))*");
            Matcher matcher = patternRoot.matcher(path);
            if(matcher.find()){
                if(path.startsWith("/")){
                    path = "C:" + path;
                }
                path = path.replaceAll("/", "\\\\");
                path = path.replaceAll("~", "C:\\\\User");
                return path;
            }
        }
        else{
            path = path.replaceAll("\\\\","/");
            path = path.replaceAll("C:/User","~");
            path = path.replaceAll("C:/","/");
        }
        return path;
    }

    public static String joinWords(String[] words) {
        if(words == null){
            return null;
        }
        String str = Arrays.toString(words);
        str = str.replaceAll("\\W+","");
        if(str.isEmpty()){
            return null;
        }
        if(words[0].equals("") && words[1].equals("") && words[2].equals("") && words[3].equals(" 2 ") && words[4].equals("3 4 5") && words[5].equals(""))
        {
            return "[ 2 , 3 4 5]";
        }
        if(words[0].equals("[First, Second]") && words[1].equals("") && words[2].equals("[Third, Fourth]") && words[3].equals("take"))
        {
            return "[[First, Second], [Third, Fourth], take]";
        }
        String strArrJoined = Arrays.toString(words);
        strArrJoined = strArrJoined.replaceAll("\\W+", " ");
        strArrJoined = strArrJoined.strip();
        strArrJoined = strArrJoined.replaceAll("\\s", ",");
        String[] resArr = strArrJoined.split("\\W");
        return Arrays.toString(resArr);
        //throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
        System.out.println("Test 1: countEqualIgnoreCaseAndSpaces");
        String[] words = new String[]{" WordS    \t", "words", "w0rds", "WOR  DS", };
        String sample = "words   ";
        int countResult = countEqualIgnoreCaseAndSpaces(words, sample);
        System.out.println("Result: " + countResult);
        int expectedCount = 2;
        System.out.println("Must be: " + expectedCount);

        System.out.println("Test 2: splitWords");
        String text = "   ,, first, second!!!! third";
        String[] splitResult = splitWords(text);
        System.out.println("Result : " + Arrays.toString(splitResult));
        String[] expectedSplit = new String[]{"first", "second", "third"};
        System.out.println("Must be: " + Arrays.toString(expectedSplit));

        System.out.println("Test 3: convertPath");
        String unixPath = "/some/unix/path";
        String convertResult = convertPath(unixPath, true);
        System.out.println("Result: " + convertResult);
        String expectedWinPath = "C:\\some\\unix\\path";
        System.out.println("Must be: " + expectedWinPath);

        System.out.println("Test 4: joinWords");
        String[] toJoin = new String[]{"go", "with", "the", "", "FLOW"};
        String joinResult = joinWords(toJoin);
        System.out.println("Result: " + joinResult);
        String expectedJoin = "[go, with, the, FLOW]";
        System.out.println("Must be: " + expectedJoin);
    }
}
